<?php

/**
 * Basic PHP form processing handling for requests from FormEntry Touch.
 *
 * This file can be uploaded to a web server that supports PHP and
 * PostgreSQL if using a database. See configurations below.
 *
 * PHP versions 4 and 5
 *
 * Widget Press, Inc (tm) :  FormEntry for Mac, FormEntry Touch, FormEntry Server (http://www.widgetpress.com)
 * Copyright 2010, Widget Press, Inc. (http://www.widgetpress.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2010, Widget Press, Inc. (http://www.widgetpress.com)
 * @link          http://www.widgetpress.com/formentry
 * @since         Generated from FormEntry for Mac 2.0.6
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Form Processing Configurations
 */
/**
 * Global set up
 */
	$year_month_day = date('Ymd'); // Displays 20100228

	$host = 'localhost';
	$fileTest = dirname( dirname(__FILE__)).'\\Preprod.ini';
	if(!file_exists($fileTest))
	{
		$fileTest = dirname( dirname(__FILE__)).'\\ProdOuTest.ini';
		if(file_exists($fileTest))
		{
			// On est en test
			$host = '192.168.0.51';
		}
	}

	$user_name = 'admin';
	$password = 'postgres';
	$media_items = array();
	$updates = '';

	$basededonnees = 'techeol';
	$schema_table_nom = 'form.timesheetcdep';
	$table_nom = 'timesheetcdep';

/**
 * PostgreSQL database set up for bdo_sred
 */
	$postgresql = true; // true or false ('true' is to use database, 'false' does not)
	$postgresql_host = $host; // database host name, typically, localhost
	$postgresql_db_name = $basededonnees; // database name
	$postgresql_user_name = $user_name; // user name to database
	$postgresql_password = $password; // password for user

	postgreSQLConnection($postgresql_host,$postgresql_user_name,$postgresql_password,$postgresql_db_name);
	pg_set_client_encoding("UTF-8");
	//pg_set_client_encoding("WIN1252");

/**
 * PostgreSQL database set up for target company
 */
	$postgresql = true; // true or false ('true' is to use database, 'false' does not)
	$postgresql_host = $host; // database host name, typically, localhost
	$postgresql_db_name = $basededonnees; // database name
	$postgresql_user_name = $user_name; // user name to database
	$postgresql_password = $password; // password for user

/**
 * Media processing set up
 */
	$media = true; // true or false ('true' is to process media files, 'false' does not)
	$media_folder = 'photo'; // folder to store media files, ie (jpg, png, etc.)


/**
 * The request is a POST method, then proceed with processing,
 */
	ksort($_POST);
	if (!isset($_POST['device_uuid8']))
	{
		$device_uuid8 = 'unknown';
	}
	else
	{
		$device_uuid8 = $_POST['device_uuid8'];
	}
	if (!isset($_POST['form_id']))
	{
		$form_id = 'unknown';
	}
	else
	{
		$form_id = $_POST['form_id'];
	}

	$hour_min_sec = date('His');


/*
 * Process media files, if set to 'true'
 */

	if ($media)
	{
		if (isset($_FILES))
		{
			ksort($_FILES);
			// Lire certaines zones
			$value = repl_date(repl_carac_franc($_POST['timesheet_date']));
			$value = date('Y-m-d', strtotime($value));
			$timesheet_date = $value;
			// Retrouver le dossier parent = dirname( dirname(__FILE__))
			$media_path = dirname( dirname(__FILE__)).substr($timesheet_date,0,4).'\\'.$media_folder;
			if (!makeDirectory($media,$media_path)) {
				exit();
			}

			$media_list = '' ;

			foreach($_FILES as $key => $media_file)
			{
				$media_items[$key] = '';
				if($media_file['size'] > 0)
				{
				    $entry_id = $_POST['entry_id'];
				    $media_file_name = $timesheet_date_inspection.'.'.$no_vehicule.'.'.$entry_id.'.'.$media_file['name'];
					$media_items[$key] = $media_file_name;
					$media_file_path = $media_path.'/'.$media_file_name;
					if (!move_uploaded_file($media_file['tmp_name'],$media_file_path))
					{
						xml_response('false','true','Remote Server Error','Could not move uploaded remote file. Please try again later.','OK');
						exit();
					}
					$media_list .= $media_file_name.',';
				}
			}

			if ($media_list != '' )
			{
				$media_list = substr($media_list, 0, strlen($media_list)-1);
				$insertPhoto = "";
				$insertPhoto .= "'".$_POST['entry_id']."', ";
				$insertPhoto .= "'".$media_list."', ";
				$insertPhoto .= "'".substr($timesheet_date_inspection,0,4)."'";
				//xml_response('false','true','query','insertPhoto = '.$insertPhoto.' ','OK');
				//exit();
				postgreSQLConnection($postgresql_host,$postgresql_user_name,$postgresql_password,$postgresql_db_name);
				pg_set_client_encoding("UTF-8");
				$query =  "DELETE from equip.timesheet_photo WHERE photo_index = '".$_POST['entry_id']."'";
	            $result = pg_query($query) or die(pg_last_error());
	            $query =  "INSERT INTO equip.timesheet_photo (photo_index, photo_valeur, photo_annee) VALUES (".$insertPhoto.");";
	            $result = pg_query($query) or die(pg_last_error());
				pg_close();
			}

		}
	}

xml_response('true','true','Succ&#232;s','Votre formulaire a &#233;t&#233; enregistr&#233; avec succ&#232;s. Merci.','OK');



/**
 * Remplacer les caract�res fran�ais
 *
 */
	function repl_carac_franc($chaine) {

        $chaine_trav = $chaine;
        /*
	    $chaine_trav = str_replace("à", "�", $chaine_trav);
	    $chaine_trav = str_replace("â", "�", $chaine_trav);
	    $chaine_trav = str_replace("é", "�", $chaine_trav);
	    $chaine_trav = str_replace("è", "�", $chaine_trav);
	    $chaine_trav = str_replace("ê", "�", $chaine_trav);
	    $chaine_trav = str_replace("ë", "�", $chaine_trav);
	    $chaine_trav = str_replace("î", "�", $chaine_trav);
	    $chaine_trav = str_replace("ô", "�", $chaine_trav);
	    $chaine_trav = str_replace("ô", "�", $chaine_trav);
	    $chaine_trav = str_replace("ù", "�", $chaine_trav);
	    $chaine_trav = str_replace("û", "�", $chaine_trav);
	    $chaine_trav = str_replace("ç", "�", $chaine_trav);

	    $chaine_trav = str_replace("À", "�", $chaine_trav);
	    $chaine_trav = str_replace("Â", "�", $chaine_trav);
	    $chaine_trav = str_replace("É", "�", $chaine_trav);
	    $chaine_trav = str_replace("È", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ê", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ë", "�", $chaine_trav);
	    $chaine_trav = str_replace("Î", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ô", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ö", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ù", "�", $chaine_trav);
	    $chaine_trav = str_replace("û", "�", $chaine_trav);
	    $chaine_trav = str_replace("Ç", "�", $chaine_trav);

	    $chaine_trav = str_replace("°", "�", $chaine_trav);
	    */
		return $chaine_trav;
	}


/**
 * Reformatter la date
 *
 */
	function repl_date($chaine) {

        $chaine_trav = $chaine;

	    $chaine_trav = str_replace("janvier"  , "january"  , $chaine_trav);
	    $chaine_trav = str_replace("f�vrier"  , "february" , $chaine_trav);
	    $chaine_trav = str_replace("mars"     , "march"    , $chaine_trav);
	    $chaine_trav = str_replace("avril"    , "april"    , $chaine_trav);
	    $chaine_trav = str_replace("mai"      , "may"      , $chaine_trav);
	    $chaine_trav = str_replace("juin"     , "june "    , $chaine_trav);
	    $chaine_trav = str_replace("juillet"  , "july"     , $chaine_trav);
	    $chaine_trav = str_replace("ao�t"     , "august"   , $chaine_trav);
	    $chaine_trav = str_replace("septembre", "september", $chaine_trav);
	    $chaine_trav = str_replace("octobre"  , "october"  , $chaine_trav);
	    $chaine_trav = str_replace("novembre" , "november" , $chaine_trav);
	    $chaine_trav = str_replace("d�cembre" , "december" , $chaine_trav);

		return $chaine_trav;
	}

function postgreSQLConnection($postgresql_host,$postgresql_user_name,$postgresql_password,$postgresql_db_name)
{
	$connectParam = 'host='.$postgresql_host.' port=5432 dbname='.$postgresql_db_name.' user='.$postgresql_user_name.' password='.$postgresql_password;
	if (!pg_connect($connectParam)) {
		xml_response('false','true','Remote Server Error','Could not select the database in PostgreSQL. Please try again later.','OK');
		exit();
	}
}

/**
 * Send XML Response.
 *
 * @param string $success String for success or failure ('true' is success, 'false' is failure)
 * @param string $show_alert String for showing alert on FormEntry Touch ('true' to show, 'false' to not show)
 * @param string $title String for displaying title of alert on FormEntry Touch
 * @param string $message String for displaying body message of alert on FormEntry Touch
 * @param string $button_label String for displaying button label of alert on FormEntry Touch
 */
	function xml_response($success = 'false', $show_alert = 'true', $title = 'Not Successful', $message = 'The post was not successfully.', $button_label = 'OK')
	{
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo "<response>\n";
		echo "	<success>".$success."</success>\n";
		echo "	<show_alert>".$show_alert."</show_alert>\n";
		echo "	<title>".$title."</title>\n";
		echo "	<message>".$message."</message>\n";
		echo "	<button_label>".$button_label."</button_label>\n";
		echo "</response>\n";
	}

function read_form_dir($db_type)
{
	$data_dir = opendir("data");
	echo '<ul>';
	while ($data_folder = readdir($data_dir)) {
		if ($data_folder!= "." && $data_folder!= ".." && is_dir('data/'.$data_folder)) {
			echo '<li><a href=" ".$table_nom.".php?db='.$db_type.'&amp;form_id='.$data_folder.'">'.$data_folder.'</a></li>';
		}
	}
	echo '</ul>';
	closedir($data_dir);
}

/**
 * Create Simple Item Array.
 *
 * @param String either head or val.
 * @param Array the $_POST array.
 * @param Array the media items array to append.
 * @param String add the 'created' column with NOW() (PostgreSQL) or Date for CSV or NULL.
 * @return Array of items.
 */
function simple_array($type='head', $post = array(), $media = array(), $created = NULL)
{
	$items = array();
	foreach($post as $head => $val) {
		if ($head !="submit" && $head != "Submit" && substr($head,0,1) != "p") {
			if ($type == 'head') {
				$items[] = $head;
			} else {
				$items[] = "'".substr($val, 0, 100)."'";
			}
		}
	}
	if (count($media) > 0) {
		foreach($media as $media_head => $media_val) {
	    	if ($head !="submit" && $head != "Submit" && substr($head,0,1) != "p") {
				if ($type == 'head') {
					$items[] = $media_head;
				} else {
					$items[] = "'".substr($media_val, 0, 100)."'";
				}
			}
		}
	}
	if ($created == "NOW()") {
		if ($type == 'head') {
			$items[] = 'created';
		} else {
			$items[] = $created;
		}
	} else if ($created != NULL) {
		if ($type == 'head') {
			$items[] = 'created';
		} else {
			$items[] = $created;
		}
	}
	return $items;
}
/**
 * Create CSV (Excel Friendly).
 *
 * @return True if successful in creation.
 */
function mssafe_csv($filepath, $data, $header = array())
{
$create_header = true;
if(file_exists($filepath)) {
	$create_header = false;
}
if ($fp = fopen($filepath, 'a')) {
	if ($create_header) {
		fputcsv($fp, $header,',','"');
	} else {
		fputcsv($fp, $data,',','"');
	}
	fclose($fp);
} else {
	return false;
}
return true;
}
/**
 * Create directory if true.
 *
 * @return False if the directory could not be created
 */
	function makeDirectory($status,$path) {
		if($status) {
			if (!is_dir($path)) {
				$rs = @mkdir($path, 0777,true);
				if(!$rs){
					xml_response('false','true','Remote Server Error',$path.' directory cannot be created. Please try again later.','OK');
					return false;
				}
			}
		}
		return true;
	}


?>