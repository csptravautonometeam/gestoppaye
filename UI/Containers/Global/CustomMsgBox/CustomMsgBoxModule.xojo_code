#tag Module
Protected Module CustomMsgBoxModule
	#tag Property, Flags = &h0
		pointeurLockModal As LockModal
	#tag EndProperty


	#tag Constant, Name = PAIE, Type = String, Dynamic = True, Default = \"Paie\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Paie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Paie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Payroll"
	#tag EndConstant

	#tag Constant, Name = ROWLOCKED, Type = String, Dynamic = True, Default = \"Data Locked by ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Data Locked by "
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Data Locked by "
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Donn\xC3\xA9es verrouill\xC3\xA9es par "
	#tag EndConstant

	#tag Constant, Name = UPDATEMODAL, Type = String, Dynamic = True, Default = \"Donn\xC3\xA9es mises \xC3\xA0 jour.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Donn\xC3\xA9es mises \xC3\xA0 jour."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Data updated."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Donn\xC3\xA9es mises \xC3\xA0 jour."
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
