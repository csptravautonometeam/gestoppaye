#tag Module
Protected Module EmployeModule
	#tag Constant, Name = CLEETRANGERE, Type = String, Dynamic = True, Default = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cl\xC3\xA9 \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Foreign key"
	#tag EndConstant

	#tag Constant, Name = CODEPOSTAL, Type = String, Dynamic = True, Default = \"Code postal", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code postal"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Postal code"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code postal"
	#tag EndConstant

	#tag Constant, Name = COMPTE, Type = String, Dynamic = True, Default = \"Compte", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Compte"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Account"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Compte"
	#tag EndConstant

	#tag Constant, Name = COURRIEL, Type = String, Dynamic = True, Default = \"Courriel", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Courriel"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Email"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Courriel"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = False, Default = \"equip", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = False, Default = \"paramequip", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DESCRIPTIONANGLAISE, Type = String, Dynamic = True, Default = \"Description anglaise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Description anglaise"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Description anglaise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"English description"
	#tag EndConstant

	#tag Constant, Name = DESCRIPTIONFRANCAISE, Type = String, Dynamic = True, Default = \"Description fran\xC3\xA7aise", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Description fran\xC3\xA7aise"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Description fran\xC3\xA7aise"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"French description"
	#tag EndConstant

	#tag Constant, Name = EMPLOYE, Type = String, Dynamic = True, Default = \"Employ\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = EMPLOYEEXISTEDEJA, Type = String, Dynamic = True, Default = \"Employ\xC3\xA9 existe d\xC3\xA9j\xC3\xA0", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employ\xC3\xA9 existe d\xC3\xA9j\xC3\xA0"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9 existe d\xC3\xA9j\xC3\xA0"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee already exists"
	#tag EndConstant

	#tag Constant, Name = EMPLOYEPREFIX, Type = String, Dynamic = True, Default = \"employe", Scope = Public
	#tag EndConstant

	#tag Constant, Name = EMPLOYETABLENAME, Type = String, Dynamic = True, Default = \"employe", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = True, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = True, Default = \"Enregistrer les modifications.", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = INACTIF, Type = String, Dynamic = True, Default = \"Inactif", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Inactif"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Inactif"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Inactive"
	#tag EndConstant

	#tag Constant, Name = NAS, Type = String, Dynamic = True, Default = \"No. Assurance sociale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. Assurance sociale"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Social Insurance number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. Assurance sociale"
	#tag EndConstant

	#tag Constant, Name = NOCIVIQUE, Type = String, Dynamic = True, Default = \"No. civique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"No. civique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Street number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. civique"
	#tag EndConstant

	#tag Constant, Name = NOM, Type = String, Dynamic = True, Default = \"Nom", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom"
	#tag EndConstant

	#tag Constant, Name = NOUVEAUNAS, Type = String, Dynamic = True, Default = \"999999999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NOUVEAUNOM, Type = String, Dynamic = True, Default = \"Nouveau nom", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouveau nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New last name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouveau nom"
	#tag EndConstant

	#tag Constant, Name = NOUVEAUPRENOM, Type = String, Dynamic = True, Default = \"Nouveau pr\xC3\xA9nom", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouveau pr\xC3\xA9nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New first name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouveau pr\xC3\xA9nom"
	#tag EndConstant

	#tag Constant, Name = NOUVELLECLE, Type = String, Dynamic = True, Default = \"Nouvelle cl\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nouvelle cl\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New key"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouvelle cl\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = PRENOM, Type = String, Dynamic = True, Default = \"Pr\xC3\xA9nom", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Pr\xC3\xA9nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"First name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pr\xC3\xA9nom"
	#tag EndConstant

	#tag Constant, Name = PROVINCE, Type = String, Dynamic = True, Default = \"Province", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Province"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Province"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Province"
	#tag EndConstant

	#tag Constant, Name = RUE, Type = String, Dynamic = True, Default = \"Rue", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rue"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Street"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rue"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TABLETRANGERE, Type = String, Dynamic = True, Default = \"Table \xC3\xA9trang\xC3\xA8re", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Table \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Table \xC3\xA9trang\xC3\xA8re"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Foreign table"
	#tag EndConstant

	#tag Constant, Name = TAUXDEM, Type = String, Dynamic = True, Default = \"Temps et demi", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Temps et demi"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Temps et demi"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time and a half"
	#tag EndConstant

	#tag Constant, Name = TAUXDOU, Type = String, Dynamic = True, Default = \"Temps double", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Temps double"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Temps double"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Double time"
	#tag EndConstant

	#tag Constant, Name = TAUXREG, Type = String, Dynamic = True, Default = \"Taux r\xC3\xA9gulier", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Taux r\xC3\xA9gulier"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Taux r\xC3\xA9gulier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Regular rate"
	#tag EndConstant

	#tag Constant, Name = TELEPHONE, Type = String, Dynamic = True, Default = \"T\xC3\xA9l\xC3\xA9phone", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"T\xC3\xA9l\xC3\xA9phone"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Telephone"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"T\xC3\xA9l\xC3\xA9phone"
	#tag EndConstant

	#tag Constant, Name = TRANSIT, Type = String, Dynamic = True, Default = \"Transit", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Transit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Transit"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Transit"
	#tag EndConstant

	#tag Constant, Name = VILLE, Type = String, Dynamic = True, Default = \"Ville", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ville"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"City"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ville"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
