#tag WebPage
Begin WebContainer EmployeContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   450
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   450
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebLabel employe_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   377
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Id"
      TextAlign       =   0
      Top             =   22
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField employe_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   377
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   100
      Text            =   ""
      TextAlign       =   0
      Top             =   42
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_nom_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   ""
      TextAlign       =   0
      Top             =   100
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   123
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_nom_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.NOM"
      TextAlign       =   0
      Top             =   78
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_nas_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   47
      Text            =   "#EmployeModule.NAS"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_nas_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   3
      Text            =   ""
      TextAlign       =   0
      Top             =   43
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_statut_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.STATUT"
      TextAlign       =   0
      Top             =   22
      VerticalCenter  =   0
      Visible         =   True
      Width           =   65
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu employe_statut_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   179
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   5
      Text            =   ""
      Top             =   44
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TLLockedByEmploye
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   15
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Roger St-Arneault"
      TextAlign       =   0
      Top             =   468
      VerticalCenter  =   0
      Visible         =   False
      Width           =   140
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVLockedEmploye
      AlignHorizontal =   3
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   396
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Picture         =   1120524287
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   443
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_prenom_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   15
      Text            =   ""
      TextAlign       =   0
      Top             =   100
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_prenom_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.PRENOM"
      TextAlign       =   0
      Top             =   78
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_no_civique_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   20
      Text            =   ""
      TextAlign       =   0
      Top             =   156
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_no_civique_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.NOCIVIQUE"
      TextAlign       =   0
      Top             =   134
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_rue_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   25
      Text            =   ""
      TextAlign       =   0
      Top             =   156
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_rue_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.RUE"
      TextAlign       =   0
      Top             =   134
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_ville_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.VILLE"
      TextAlign       =   0
      Top             =   190
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_ville_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   30
      Text            =   ""
      TextAlign       =   0
      Top             =   212
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   123
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_province_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.PROVINCE"
      TextAlign       =   0
      Top             =   190
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_province_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   35
      Text            =   ""
      TextAlign       =   0
      Top             =   212
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_code_postal_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.CODEPOSTAL"
      TextAlign       =   0
      Top             =   246
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_code_postal_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   40
      Text            =   ""
      TextAlign       =   0
      Top             =   268
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   123
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_courriel_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.COURRIEL"
      TextAlign       =   0
      Top             =   302
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_courriel_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   50
      Text            =   ""
      TextAlign       =   0
      Top             =   324
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   294
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_tel_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   181
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.TELEPHONE"
      TextAlign       =   0
      Top             =   246
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_tel_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   181
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   45
      Text            =   ""
      TextAlign       =   0
      Top             =   268
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   158
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_transit_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   9
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.TRANSIT"
      TextAlign       =   0
      Top             =   358
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_transit_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   9
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   55
      Text            =   ""
      TextAlign       =   0
      Top             =   380
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   123
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_compte_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   180
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.COMPTE"
      TextAlign       =   0
      Top             =   358
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_compte_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   180
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   60
      Text            =   ""
      TextAlign       =   0
      Top             =   380
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   158
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_treg_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.TAUXREG"
      TextAlign       =   0
      Top             =   414
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_treg_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   65
      Text            =   ""
      TextAlign       =   0
      Top             =   436
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   87
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_tdem_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   131
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.TAUXDEM"
      TextAlign       =   0
      Top             =   414
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_tdem_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   131
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   70
      Text            =   ""
      TextAlign       =   0
      Top             =   436
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   87
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField employe_tdou_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   75
      Text            =   ""
      TextAlign       =   0
      Top             =   436
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   87
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel employe_tdou_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#EmployeModule.TAUXDOU"
      TextAlign       =   0
      Top             =   414
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listEmployeDetail()
		  // Cacher le Lock
		  IVLockedEmploye.Visible = False
		  TLLockedByEmploye.Visible = False
		  
		  If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.lockEditMode Then 
		    // Paramètre 1 = WebView
		    // Paramètre 2 = Initialisation du contenu des zones
		    // Paramètre 3 = Initialisation des styles
		    // Paramètre 4 = Verrouillage des zones
		    employe_statut_popupMenu.Enabled = True
		    If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas = EmployeModule.NOUVEAUNAS Then GenControlModule.resetControls(Self, False, False, False)
		    Exit Sub
		  End If
		  
		  
		  employe_statut_popupMenu.SetFocus
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye = Nil Then
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye = new EmployeClass()
		  End If
		  
		  If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Creation" Then
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Modification"
		    employe_nas_TextField.Text =Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nas
		    employe_nom_TextField.Text =Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nom
		    employe_prenom_TextField.Text =Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.prenom
		    Exit
		  End If
		  
		  // Alimenter les listes déroulantes
		  employe_statut_PopupMenu.loadData(Session.bdTechEol,  "parametre", "parametre", "statut")
		  
		  // Mode Modification, Lire les données
		  Dim employeRS As RecordSet = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.loadDataByField(Session.bdTechEol, EmployeModule.EMPLOYETABLENAME, _
		  "employe_id", str(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id), "employe_id")
		  
		  If employeRS <> Nil Then Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.LireDonneesBD(employeRS, EmployeModule.EMPLOYEPREFIX)
		  
		  employeRS.Close
		  employeRS = Nil
		  
		  // Sauvegarde
		  sauvegardeEmployeNas = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas
		  sauvegardeEmployeNom = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nom
		  sauvegardeEmployePrenom = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_prenom
		  sauvegardeEmployeStatut =  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_statut
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_id
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nas = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas
		  
		  // Assignation des propriétés aux Contrôles
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.assignPropertiesToControls(Self, EmployeModule.EMPLOYEPREFIX)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub majEmployeDetail()
		  
		  
		  If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Creation" Then
		    //alimListeDeroulante
		    // Assignation des propriétés aux Contrôles
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.assignPropertiesToControls(Self, EmployeModule.EMPLOYEPREFIX)
		  End If
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.assignControlToProperties(Self, EmployeModule.EMPLOYEPREFIX)
		  Dim messageErreur  As String = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.writeData(Session.bdTechEol, EmployeModule.EMPLOYETABLENAME, EmployeModule.EMPLOYEPREFIX)
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EmployeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  // Réafficher la liste des employes si les valeurs ont changé
		  If (sauvegardeEmployeNas <> Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas _
		    Or sauvegardeEmployeNom <> Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nom _
		    Or sauvegardeEmployePrenom <> Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_prenom _
		    Or sauvegardeEmployeStatut <>  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_statut) _
		    And Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode <> "Creation"  Then
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.populateEmploye(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_id)
		  End If
		  
		  sauvegardeEmployeNas = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas
		  sauvegardeEmployeNom = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nom
		  sauvegardeEmployePrenom = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_prenom
		  sauvegardeEmployeStatut =  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_statut
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_id
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nas = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_nas
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.statut =  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_statut
		  
		  If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Modification"
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteMAJ()
		  If validationDesZonesOK = True Then
		    majEmployeDetail
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Modification"
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validationDesZonesOK() As Boolean
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, True, False)
		  
		  Dim messageRetour as String = ""
		  
		  // Validation
		  messageRetour = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.validerZones( _
		  Session.bdTechEol, _
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.employe_id, _
		  employe_nas_TextField)
		  
		  If messageRetour <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = messageRetour
		    pointeurProbMsgModal.Show
		    
		    Return False
		  End If
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private sauvegardeEmployeNas As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeEmployeNom As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeEmployePrenom As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeEmployeStatut As String
	#tag EndProperty


#tag EndWindowCode

#tag Events employe_nom_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_prenom_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_no_civique_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_rue_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_ville_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_province_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_code_postal_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_courriel_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_tel_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_transit_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_compte_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_treg_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_tdem_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events employe_tdou_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
