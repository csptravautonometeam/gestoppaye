#tag WebPage
Begin WebContainer EmployeSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "931806350"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   500
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   500
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox EmployeListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   4
      ColumnWidths    =   "26%,35%,35%,4%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   445
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#EmployeModule.NAS	#EmployeModule.NOM	#EmployeModule.PRENOM	ST"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   500
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel EmployeLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddEmploye
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#EmployeModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   430
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1401872383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelEmploye
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#EmployeModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   464
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   415664127
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrEmploye
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#EmployeModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   380
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1113270271
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#EmployeModule.EMPLOYE"
      TextAlign       =   0
      Top             =   12
      VerticalCenter  =   0
      Visible         =   True
      Width           =   165
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModEmploye
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#EmployeModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   346
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1238587391
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox EmployeInactifCheckbox
      Caption         =   "#EmployeModule.INACTIF"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   196
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   20
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   60
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterEmploye()
		  Dim compteur As Integer
		  compteur = EmployeListBox.RowCount  'Pour Debug
		  compteur = EmployeListBox.LastIndex    'Pour Debug
		  compteur = EmployeListBox.ListIndex ' Pour Debug
		  
		  EmployeListBox.insertRow(compteur+1, EmployeModule.NOUVEAUNAS, EmployeModule.NOUVEAUNOM, EmployeModule.NOUVEAUPRENOM, "A")
		  
		  // Faire pointer le curseur sur le nouveal employe
		  EmployeListBox.ListIndex = EmployeListBox.ListIndex + 1
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne = EmployeListBox.ListIndex
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nas = EmployeModule.NOUVEAUNAS
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.nom = EmployeModule.NOUVEAUNOM
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.prenom = EmployeModule.NOUVEAUPRENOM
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.statut = "A"
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement
		  Self.sEmploye = Nil
		  Self.sEmploye = new EmployeClass()
		  Self.sEmploye.employe_nas = EmployeModule.NOUVEAUNAS
		  Self.sEmploye.employe_nom = EmployeModule.NOUVEAUNOM
		  Self.sEmploye.employe_prenom = EmployeModule.NOUVEAUPRENOM
		  Self.sEmploye.employe_statut = "A"
		  // employe_cleetr_nom et employe_cleetr_cle vont prendre la valeur de la ligne précédente
		  
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id = 0  // indique que c'est un nouvel enregistrement
		  Self.sEmploye.employe_id = 0
		  Session.pointeurEmployeWebDialog.EmployeContainerDialog.majEmployeDetail
		  
		  // Faire un reset de la séquence dbglobal.employe_id_seq
		  Dim nouvelleSequence As Integer = Self.sEmploye.employe_id+1
		  Session.bdTechEol.SQLExecute("ALTER SEQUENCE employe_id_seq RESTART " + str(nouvelleSequence))
		  
		  // Renumérotation de la listbox 
		  //renumerotation
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  populateEmploye(Self.sEmploye.employe_id)
		  
		  // Se mettre en mode lock
		  //implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementEmploye(idEmploye_param As Integer)
		  // Si on est en verrouillage et qu'il y a changement d'enregistrement, on remet les icones pertinents actifs et on bloque l'écran Container 
		  If Self.sEmploye.lockEditMode = True Then
		    Self.sEmploye.lockEditMode = False
		    // Enlever les verrouillages existants
		    Dim message As String = Self.sEmploye.cleanUserLocks(Session.bdTechEol)
		    If  message <> "Succes" Then afficherMessage(message)
		    
		    // Rendre disponible l'ajout et la suppression
		    IVModEmploye.Picture = ModModal30x30
		    IVAddEmploye.Visible = True
		    IVAddEmploye.Enabled = True
		    IVDelEmploye.Visible = True
		    IVDelEmploye.Enabled = True
		    IVEnrEmploye.Visible = False
		    IVEnrEmploye.Enabled = False
		  End If
		  
		  // Si la valeur de idEmploye  est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idEmploye_param  = 0 Then
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.edit_mode = "Creation"
		  Else
		    Self.sEmploye.listDataByField(Session.bdTechEol, EmployeModule.EMPLOYETABLENAME,EmployeModule.EMPLOYEPREFIX,  _
		    "employe_id", str(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id), "employe_nas")
		    Session.pointeurEmployeWebDialog.EmployeContainerDialog.listEmployeDetail
		  End If
		  
		  //pointeurEmployeWebDialog.EmployeContainerDialog.listEmployeDetail
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  Dim ptrEmployeCont As EmployeContainer = Session.pointeurEmployeWebDialog.EmployeContainerDialog
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = EmployeModule.DBSCHEMANAME + "." + EmployeModule.DBTABLENAME + "." + str(Self.sEmploye.employe_id)
		  Message = Self.sEmploye.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  Message <> "Locked" And Message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    ptrEmployeCont.IVLockedEmploye.Visible = True
		    ptrEmployeCont.TLLockedByEmploye.Text = Self.sEmploye.lockedBy
		    ptrEmployeCont.TLLockedByEmploye.Visible = True
		    Exit Sub
		  End If
		  
		  //Déverrouiller l'enregistrement si on est actuellement en Verrouillage
		  If Self.sEmploye.lockEditMode = True Then
		    message = Self.sEmploye.unLockRow(Session.bdTechEol)
		    If  message <> "Succes" Then
		      afficherMessage(message)
		      Exit Sub
		    End If
		    IVModEmploye.Picture = ModModal30x30
		    // Rendre disponible l'ajout et la suppression
		    IVAddEmploye.Visible = True
		    IVAddEmploye.Enabled = True
		    IVDelEmploye.Visible = True
		    IVDelEmploye.Enabled = True
		    // Libérer 
		    ptrEmployeCont.IVLockedEmploye.Visible = False
		    ptrEmployeCont.TLLockedByEmploye.Visible = False
		    IVEnrEmploye.Visible = False
		    IVEnrEmploye.Enabled = False
		    // Bloquer les zones du container
		    Self.sEmploye.lockEditMode = False
		    Session.pointeurEmployeWebDialog.EmployeContainerDialog.listEmployeDetail
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sEmploye.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sEmploye.recordID = EmployeModule.DBSCHEMANAME + "." +EmployeModule.DBTABLENAME+ "." + str(Self.sEmploye.employe_id)
		  message = Self.sEmploye.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  ptrEmployeCont.IVLockedEmploye.Visible = False
		  ptrEmployeCont.TLLockedByEmploye.Visible = False
		  IVEnrEmploye.Enabled = Self.sEmploye.recordAvailable
		  
		  //Activer les fonctions pertinentes
		  IVModEmploye.Picture = ModModalOrange30x30
		  IVAddEmploye.Visible = False
		  IVAddEmploye.Enabled = False
		  IVDelEmploye.Visible = False
		  IVDelEmploye.Enabled = False
		  IVEnrEmploye.Visible = True
		  IVEnrEmploye.Enabled = True
		  
		  // Débloquer les zones du container
		  Session.pointeurEmployeWebDialog.EmployeContainerDialog.listEmployeDetail
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modEmployeDetail()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateEmploye(Optional employe_id_param as Integer)
		  
		  If Self.sEmploye = Nil Then
		    Self.sEmploye = new EmployeClass()
		  End If
		  
		  Dim employeRS As RecordSet
		  employeRS = Self.sEmploye.loadData(Session.bdTechEol, EmployeModule.EMPLOYETABLENAME, "employe_nom", "employe_prenom")
		  
		  EmployeListBox.DeleteAllRows
		  
		  If employeRS = Nil Then Exit Sub
		  
		  Dim index As Integer = 0
		  For i As Integer = 1 To employeRS.RecordCount
		    If Not (employeRS.Field("employe_statut").StringValue = "I" And Me.EmployeInactifCheckbox.Value = False) Then
		      
		      EmployeListBox.AddRow(employeRS.Field("employe_nas").StringValue, employeRS.Field("employe_nom").StringValue, _
		      employeRS.Field("employe_prenom").StringValue, employeRS.Field("employe_statut").StringValue)
		      
		      If employeRS.Field("employe_statut").StringValue <> "A" Then
		        For j As Integer = 0 To 3
		          EmployeListBox.CellStyle(i-1, j) = BGGray200
		        Next
		      End If
		      
		      // Garder le numéro de ligne
		      EmployeListBox.RowTag(EmployeListBox.LastIndex) = index
		      // Garder l'id numéro du employe
		      EmployeListBox.CellTag(EmployeListBox.LastIndex,1) = employeRS.Field("employe_id").IntegerValue
		      index = index + 1
		    End If
		    employeRS.MoveNext
		  Next
		  
		  employeRS.Close
		  employeRS = Nil
		  
		  // Si la liste est vide, sortir
		  If EmployeListBox.RowCount <= 0 Then Exit Sub
		  
		  
		  // Positionnement par rapport à un numéro en particulier
		  If employe_id_param <> 0 Then
		    For indexListBox As Integer = 0 To EmployeListBox.RowCount - 1
		      If EmployeListBox.CellTag(indexListBox,1) = employe_id_param Then
		        Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne = indexListBox
		        EmployeListBox.ListIndex  = indexListBox
		        Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id= EmployeListBox.CellTag(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne,1)
		        Exit Sub
		      End If
		    Next
		    Exit Sub
		  End If
		  
		  // Positionnement à la ligne gardée en mémoire
		  If EmployeListBox.ListIndex  <> Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne And EmployeListBox.RowCount > 0 Then
		    Dim v As Variant = EmployeListBox.LastIndex
		    v = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne
		    If Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne > EmployeListBox.LastIndex  Then 
		      Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne = EmployeListBox.LastIndex
		    End If
		    EmployeListBox.ListIndex  = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne
		    v = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne
		    v = EmployeListBox.CellTag(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne,1)
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id = EmployeListBox.CellTag(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne,1)
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub renumerotation()
		  //Dim employeRS As RecordSet
		  //Dim compteur As Integer = 0
		  //
		  //employeRS = Self.sEmploye.loadData(Session.bdTechEol, EmployeModule.EMPLOYETABLENAME, "employe_nom", "employe_prenom")
		  //
		  //While Not employeRS.EOF
		  //
		  //Self.sEmploye.LireDonneesBD(employeRS, EmployeModule.EMPLOYEPREFIX)
		  //Self.sEmploye.employe_tri = compteur
		  //Dim messageErreur  As String = Self.sEmploye.writeData(Session.bdTechEol, EmployeModule.EMPLOYETABLEName, EmployeModule.EMPLOYEPREFIX)
		  //If (messageErreur <> "Succes") Then
		  //Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		  //pointeurProbMsgModal.WLAlertLabel.Text = EmployeModule.ERREUR + " " + messageErreur
		  //pointeurProbMsgModal.Show
		  //Exit
		  //End If
		  //
		  //compteur = compteur+1
		  //employeRS.MoveNext
		  //Wend
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supEmployeDetail()
		  Dim ptrEmployeCont As EmployeContainer = Session.pointeurEmployeWebDialog.EmployeContainerDialog
		  
		  // Vérifier si un enregistrement peut être supprimé
		  Dim messageErreur as String = Self.sEmploye.validerSuppression(Session.bdTechEol, Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.table_nom, Self.sEmploye.employe_nas)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EmployeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = EmployeModule.DBSCHEMANAME + "." +EmployeModule.DBTABLENAME+ "." + str(Self.sEmploye.employe_id)
		  messageErreur = Self.sEmploye.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    ptrEmployeCont.IVLockedEmploye.Visible = True
		    ptrEmployeCont.TLLockedByEmploye.Text = Self.sEmploye.lockedBy
		    ptrEmployeCont.TLLockedByEmploye.Visible = True
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id
		  messageErreur = Self.sEmploye.supprimerDataByField(Session.bdTechEol, EmployeModule.EMPLOYETABLEName, "employe_id",str(Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = EmployeModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des employes
		    Self.renumerotation
		    Self.populateEmploye
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		employeStruct As EmployeStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sEmploye As EmployeClass
	#tag EndProperty


	#tag Structure, Name = employeStructure, Flags = &h0
		nas As String*30
		  prenom As String*100
		  nom As String*100
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		statut As String*1
	#tag EndStructure


#tag EndWindowCode

#tag Events EmployeListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex >= 0 Then
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementEmploye(Me.CellTag(Me.ListIndex,1))
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddEmploye
	#tag Event
		Sub MouseExit()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddEmploye.Picture = AjouterModalTE30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    ajouterEmploye
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelEmploye
	#tag Event
		Sub MouseExit()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModalTE30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  //If Me.Enabled Then
		  //supEmployeDetail
		  //End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrEmploye
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  Session.pointeurEmployeWebDialog.EmployeContainerDialog.traiteMAJ
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = EnregistrerTE30x30
		  Self.IVModEmploye.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModEmploye
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If EmployeListBox.ListIndex <> -1 And EmployeListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  If Self.sEmploye.lockEditMode = False Then
		    Self.IVModEmploye.Picture = ModModal30x30
		  Else
		    Self.IVModEmploye.Picture = ModModalOrange30x30
		  End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddEmploye.Picture = AjouterModal30x30
		  Self.IVDelEmploye.Picture = DeleteModal30x30
		  Self.IVEnrEmploye.Picture = Enregistrer30x30
		  Self.IVModEmploye.Picture = ModModalTE30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events EmployeInactifCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des employés
		  populateEmploye
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
