#tag Class
Protected Class EmployeClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, EMPLOYETABLENAME, EMPLOYEPREFIX)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(employeDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  Select Case table_nom
		    
		  Case "reppale_descanomalie"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		  Case "reppale_etat"
		    strSQL = "SELECT * FROM reppale WHERE reppale_rep_etat = '" + value + "'"
		  Case "reppale_gravite"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_gravite = '" + value + "'"
		  Case "reppale_intervention"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_intervention = '" + value + "'"
		  Case "reppale_acces"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_methode_acces = '" + value + "'"
		  Case "reppale_pospremiere"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_premiere = '" + value + "'"
		  Case "reppale_posseconde"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_seconde = '" + value + "'"
		  Case "reppale_recouvrement"
		    strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '" + value + "'"
		    If value = "NS" Then strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '' Or  reppale_recouvrement = '" + value + "'"
		  Case "responsable"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_responsable = '" + value + "'"
		  Case "sitecode"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_site = '" + value + "'"
		  Case "reppale_statut"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_statut = '" + value + "'"
		  Case "reppale_typeanomalie"
		    // strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    
		  End Select
		  
		  recordSet =  employeDB .SQLSelect(strSQL)
		  Dim compteur As Integer = recordSet.RecordCount
		  
		  If compteur > 0 Then
		    Return EmployeModule.ENREGNEPEUTPASETREDETRUIT
		  Else
		    Return  "Succes"
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(employeDB As PostgreSQLDatabase,  employe_id_param As Integer, ByRef employe_nas_param As WebTextField) As String
		  
		  Dim employeRS As RecordSet = Session.pointeurEmployeWebDialog.EmployeSideBarDialog.sEmploye.loadDataByField(Session.bdTechEol, EMPLOYETABLENAME, _
		  "employe_nas", employe_nas_param.Text, "employe_nas")
		  If employeRS = Nil Then GoTo Suivant
		  
		  Dim compteur As Integer = employeRS.RecordCount
		  Dim employe_id_extrait As Integer = employeRS.Field("employe_id").IntegerValue
		  employeRS.Close
		  employeRS = Nil
		  
		  If compteur > 0 Then
		    // Si on a trouvé un employé avec le numéro de NAS et que l'ID ne correspond pas, c'est qu'on essaie d'ajouter un autre employé avec le même numéro de NAS 
		    If employe_id_param <> employe_id_extrait Then
		      Return EmployeModule.EMPLOYEEXISTEDEJA
		    End If
		  End If
		  
		  Suivant:
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		employe_code_postal As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_compte As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_courriel As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_no_civique As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_prenom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_province As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_rue As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_statut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tdem As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tdou As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_tel As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_transit As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_treg As String
	#tag EndProperty

	#tag Property, Flags = &h0
		employe_ville As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="employe_code_postal"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_compte"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_courriel"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_no_civique"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_prenom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_province"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_rue"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_statut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tdem"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tdou"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_tel"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_transit"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_treg"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="employe_ville"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
