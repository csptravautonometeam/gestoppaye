#tag Module
Protected Module PaieModule
	#tag Constant, Name = ANNEE, Type = String, Dynamic = True, Default = \"Ann\xC3\xA9e", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ann\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Year"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ann\xC3\xA9e"
	#tag EndConstant

	#tag Constant, Name = CODEPAIE, Type = String, Dynamic = True, Default = \"Code de paie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code de paie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code de paie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pay code"
	#tag EndConstant

	#tag Constant, Name = COULEUR, Type = String, Dynamic = True, Default = \"Couleur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Couleur"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Couleur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Color"
	#tag EndConstant

	#tag Constant, Name = DATECUE, Type = String, Dynamic = True, Default = \"AAAA-MM-JJ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"AAAA-MM-JJ\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"AAAA-MM-JJ"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"YYYY-MM-DD"
	#tag EndConstant

	#tag Constant, Name = DATEDEFIN, Type = String, Dynamic = True, Default = \"Date de fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin"
	#tag EndConstant

	#tag Constant, Name = DATEDUE, Type = String, Dynamic = True, Default = \"Date due", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date due"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Due date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date due"
	#tag EndConstant

	#tag Constant, Name = DATEPAIE, Type = String, Dynamic = True, Default = \"Date de paie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de paie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Payroll date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de paie"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = True, Default = \"dbglobal", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"timesheetcdep", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DETAIL, Type = String, Dynamic = True, Default = \"D\xC3\xA9tail", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Detail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9tail"
	#tag EndConstant

	#tag Constant, Name = DETAILPAIE, Type = String, Dynamic = True, Default = \"D\xC3\xA9tail \xC3\xA9quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9tail \xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment detail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9tail \xC3\xA9quipement"
	#tag EndConstant

	#tag Constant, Name = EMPLACEMENT, Type = String, Dynamic = True, Default = \"Emplacement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Emplacement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Emplacement"
	#tag EndConstant

	#tag Constant, Name = EMPLOYE, Type = String, Dynamic = True, Default = \"Employ\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Employ\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = ENREGNEPEUTPASETREDETRUIT, Type = String, Dynamic = True, Default = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement ne peut \xC3\xAAtre d\xC3\xA9truit. Il est encore contenu dans un enregistrement de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record cannot be deleted. It is still part of a database record."
	#tag EndConstant

	#tag Constant, Name = ENREGVERROUILLEPAR, Type = String, Dynamic = True, Default = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur ", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur \r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrement est verrouill\xC3\xA9 par l\'utilisateur \r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Record locked by user "
	#tag EndConstant

	#tag Constant, Name = ERREUR, Type = String, Dynamic = True, Default = \"Erreur:", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur:"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Error:"
	#tag EndConstant

	#tag Constant, Name = FACTURABLE, Type = String, Dynamic = True, Default = \"Facturable", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Facturable"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Facturable"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Billable"
	#tag EndConstant

	#tag Constant, Name = HEURESMONTANT, Type = String, Dynamic = True, Default = \"Heures/ Montant", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Heures/ Montant"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Heures/ Montant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Hours/ Amount"
	#tag EndConstant

	#tag Constant, Name = HINTADD, Type = String, Dynamic = True, Default = \"Ajouter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
	#tag EndConstant

	#tag Constant, Name = HINTLOCKUNLOCK, Type = String, Dynamic = False, Default = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Lock/Unlock record"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Verrouiller/D\xC3\xA9verrouiller l\'enregistrement\r"
	#tag EndConstant

	#tag Constant, Name = HINTSUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
	#tag EndConstant

	#tag Constant, Name = HINTUPDATEDETAILS, Type = String, Dynamic = False, Default = \"Enregistrer les modifications.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Enregistrer les modifications."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Save modifications."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Enregistrer les modifications."
	#tag EndConstant

	#tag Constant, Name = LISTE, Type = String, Dynamic = True, Default = \"Liste", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Liste"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"List"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Liste"
	#tag EndConstant

	#tag Constant, Name = LOCALISATION, Type = String, Dynamic = True, Default = \"Localisation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Localisation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Localisation"
	#tag EndConstant

	#tag Constant, Name = LOCALISATIONDATE, Type = String, Dynamic = True, Default = \"Date de fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date de fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date de fin"
	#tag EndConstant

	#tag Constant, Name = NAS, Type = String, Dynamic = True, Default = \"NAS", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"NAS"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SAN"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"NAS"
	#tag EndConstant

	#tag Constant, Name = NOTHING, Type = String, Dynamic = True, Default = \"Rien", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rien"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Nothing"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rien"
	#tag EndConstant

	#tag Constant, Name = PAIE, Type = String, Dynamic = True, Default = \"Paie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Paie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Payroll"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Paie"
	#tag EndConstant

	#tag Constant, Name = PAIEDETAIL, Type = String, Dynamic = True, Default = \"Paie d\xC3\xA9tail", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Paie d\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Payroll detail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Paie d\xC3\xA9tail"
	#tag EndConstant

	#tag Constant, Name = PAIEPREFIX, Type = String, Dynamic = True, Default = \"timesheetcdep", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PAIETABLENAME, Type = String, Dynamic = True, Default = \"timesheetcdep", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PAYE, Type = String, Dynamic = True, Default = \"Pay\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Pay\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Paid"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pay\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = String, Dynamic = True, Default = \"Projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet"
	#tag EndConstant

	#tag Constant, Name = PURCHASEORDERNO, Type = String, Dynamic = True, Default = \"No. bon de commande", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"#Bon de commande"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"#Purchase Order"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"#Bon de commande"
	#tag EndConstant

	#tag Constant, Name = REQUIS, Type = String, Dynamic = True, Default = \"Required", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Requis"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = REVISION, Type = String, Dynamic = True, Default = \"R\xC3\xA9vision", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vision"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revision"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vision"
	#tag EndConstant

	#tag Constant, Name = SELECTIONRECORDADETRUIRE, Type = String, Dynamic = True, Default = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"S\xC3\xA9lectionner au moins un enregistrement \xC3\xA0 d\xC3\xA9truire."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Select at least one record to delete."
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TYPEPAIE, Type = String, Dynamic = True, Default = \"Type d\'\xC3\xA9quipement", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment type"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type d\'\xC3\xA9quipement"
	#tag EndConstant

	#tag Constant, Name = UNITENO, Type = String, Dynamic = True, Default = \"#Unit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"#Unit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Unit#"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"#Unit\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = VALEUR, Type = String, Dynamic = True, Default = \"Valeur\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Valeur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Value"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Valeur"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant

	#tag Constant, Name = ZONESOBLIGATOIRES, Type = String, Dynamic = True, Default = \"Required field(s)", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Required field(s)"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Zone(s) obligatoire(s)"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Required field(s)"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
