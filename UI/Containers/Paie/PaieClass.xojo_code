#tag Class
Protected Class PaieClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h1000
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, PAIETABLENAME ,paieprefix)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWherePremier(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		      
		    Case "DateDebut"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and timesheetcdep_date_paie >= '" + v + "'  "
		      
		    Case "DateFin"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and timesheetcdep_date_paie <= '" + v + "'  "
		      
		    Case "CodePaie"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and substr(timesheetcdep_code_paie,1,1) IN(" + v + ")  "
		      
		    Case "Employe"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and timesheetcdep_nas IN('" + v + "')  "
		      
		    Case "Revision"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and timesheetcdep_revision IN(" + v + ")  "
		      
		    End Select
		    
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataPaieDetail(dB AS PostgreSQLDatabase, listeSelection_param AS Dictionary) As RecordSet
		  Dim genRS AS RecordSet
		  Dim strSQL AS  String
		  Dim compteur AS Integer
		  
		  // Génération de la clause Where
		  Dim clauseWhere AS  String = genClauseWherePremier(listeSelection_param)
		  
		  If clauseWhere <> "" Then
		    // Ajout du Where et enlever le premier And
		    clauseWhere = " WHERE " + mid(clauseWhere,4)
		  End If
		  
		  strSQL = "SELECT timesheetcdep_id, timesheetcdep_date_creation, timesheetcdep_nas, timesheetcdep_date_paie, timesheetcdep_projet, timesheetcdep_code_paie, timesheetcdep_tag_modif,"+ _
		  "(SELECT employe_nom || ' ' || employe_prenom  FROM employe where timesheetcdep_nAS = employe_nas Limit 1)  AS timesheetcdep_employe_nom,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where timesheetcdep_projet = projet_no Limit 1) AS timesheetcdep_projet_description, " + _
		  "(SELECT parametre_desc_fr  FROM parametre where parametre_nom = 'codepaieentree'   AND timesheetcdep_code_paie = parametre_cle Limit 1)  AS timesheetcdep_code_paie_description,  " + _
		  "(SELECT parametre_tri  FROM parametre where parametre_nom = 'codepaieentree'   AND timesheetcdep_code_paie = parametre_cle Limit 1)  AS timesheetcdep_code_paie_tri,  " + _
		  " timesheetcdep_nombre, timesheetcdep_detail, timesheetcdep_facturable, timesheetcdep_tag_modif,  timesheetcdep_revision, timesheetcdep_paye, " + _
		  "(SELECT parametre_desc_fr  FROM parametre where parametre_nom = 'boolean'   AND timesheetcdep_facturable = parametre_cle Limit 1)  AS timesheetcdep_facturable_description,  " + _
		  "(SELECT parametre_desc_fr  FROM parametre where parametre_nom = 'boolean'   AND timesheetcdep_revision = parametre_cle Limit 1)  AS timesheetcdep_revision_description,  " + _
		  "(SELECT parametre_desc_fr  FROM parametre where parametre_nom = 'boolean'   AND timesheetcdep_paye = parametre_cle Limit 1)  AS timesheetcdep_paye_description  " + _
		  "FROM timesheetcdep  " + _
		  clauseWhere + _
		  "ORDER BY timesheetcdep_employe_nom, timesheetcdep_date_paie, timesheetcdep_code_paie_tri, timesheetcdep_tag_modif DESC" 
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataPaieSommaire(dB AS PostgreSQLDatabase, listeSelection_param AS Dictionary) As RecordSet
		  Dim genRS AS RecordSet
		  Dim strSQL AS  String
		  Dim compteur AS Integer
		  
		  // Génération de la clause Where
		  Dim clauseWhere AS  String = genClauseWherePremier(listeSelection_param)
		  
		  If clauseWhere <> "" Then
		    // Ajout du Where et enlever le premier And
		    clauseWhere = " WHERE " + mid(clauseWhere,4)
		  End If
		  
		  strSQL = "SELECT timesheetcdep_nas, timesheetcdep_date_paie, timesheetcdep_projet, timesheetcdep_code_paie, "+ _
		  "(SELECT employe_nom || ' ' || employe_prenom  FROM employe where timesheetcdep_nas = employe_nas Limit 1)  AS timesheetcdep_employe_nom,  " + _
		  "(SELECT projet_no || ' ' || projet_titre  FROM projet where timesheetcdep_projet = projet_no Limit 1) AS timesheetcdep_projet_description, " + _
		  "(SELECT parametre_desc_fr  FROM parametre where parametre_nom = 'codepaieentree'   AND timesheetcdep_code_paie = parametre_cle Limit 1)  AS timesheetcdep_code_paie_description,  " + _
		  "(SELECT parametre_tri  FROM parametre where parametre_nom = 'codepaieentree'   AND timesheetcdep_code_paie = parametre_cle Limit 1)  AS timesheetcdep_code_paie_tri,  " + _
		  " SUM(CAST(timesheetcdep_nombre As FLOAT)) AS timesheetcdep_nombre_somme " + _
		  "FROM timesheetcdep  " + _
		  clauseWhere + _
		  "GROUP BY timesheetcdep_nas, timesheetcdep_employe_nom, timesheetcdep_date_paie, timesheetcdep_projet, timesheetcdep_code_paie_tri, timesheetcdep_code_paie "  + _
		  "ORDER BY  timesheetcdep_employe_nom, timesheetcdep_date_paie, timesheetcdep_projet, timesheetcdep_code_paie_tri " 
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(dB As PostgreSQLDatabase, paie_id As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  //strSQL = "SELECT * FROM photo WHERE  photo_index = '" + projet_id + "'" 
		  //recordSet =  dB .SQLSelect(strSQL)
		  //compteur = recordSet.RecordCount
		  //recordSet.Close
		  recordSet = Nil
		  //
		  //If compteur > 0 Then 
		  //Return Strings.ENREGNONDETRUIT
		  //End If
		  
		  Return "Succes"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(equipDB As PostgreSQLDatabase, equipm_unite_no_param As WebTextField) As String
		  
		  // Vérification des zones obligatoires
		  If equipm_unite_no_param .Text = ""   Then
		    equipm_unite_no_param .Style = TextFieldErrorStyle
		    Return PaieModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérifier si le nuo. unité commence par T-
		  //If mid(equipm_unite_no_param .Text,1,2) <> "T-"   Then
		  //equipm_unite_no_param .Style = TextFieldErrorStyle
		  //Return PaieModule.UNITENOCOMMENCEPART
		  //End If
		  
		  // Vérifier si la clé entrée (equipm_unite_no) est différente et existe déjà dans la BD
		  //If equipm_unite_no_param .Text <> Session.pointeurPaieSideBarContainer.sPaie.equipm_unite_no Then
		  //// Mode Modification, Lire les données
		  //Dim paieRS As RecordSet = Session.pointeurPaieSideBarContainer.sPaie.loadDataByField(Session.bdTechEol, paieTABLENAME, _
		  //"equipm_unite_no", equipm_unite_no_param.Text, "equipm_unite_no")
		  //If paieRS = Nil Then GoTo Suivant
		  //
		  //Dim compteur As Integer = paieRS.RecordCount
		  //paieRS.Close
		  //paieRS = Nil
		  //If compteur > 0 Then Return PaieModule.NOUNITEEXISTEDEJA
		  //End If
		  
		  Suivant:
		  
		  Return "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		timesheetcdep_code_paie As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_date_creation As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_date_paie As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_detail As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_entry_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_facturable As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_nombre As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_paye As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_revision As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timesheetcdep_tag_modif As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_code_paie"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_date_creation"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_date_paie"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_detail"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_entry_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_facturable"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_nombre"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_paye"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timesheetcdep_tag_modif"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
