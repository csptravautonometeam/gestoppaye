#tag WebPage
Begin WebDialog PaieWebDialog
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   455
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   0
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   0
   MinWidth        =   0
   Resizable       =   True
   Style           =   "None"
   TabOrder        =   0
   Title           =   "Untitled"
   Top             =   0
   Type            =   3
   VerticalCenter  =   0
   Visible         =   True
   Width           =   380
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebImageView IVUpdate
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   298
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Picture         =   544049151
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   387
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   62
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVQuitter
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   330
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1387319295
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField timesheetcdep_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   68
      LimitText       =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   200
      Text            =   ""
      TextAlign       =   0
      Top             =   413
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   56
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel PaieDetLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   33
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1436473343"
      TabOrder        =   0
      Text            =   "#PaieModule.PAIEDETAIL"
      TextAlign       =   0
      Top             =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel PaieEmployeLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   171
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   0
      Text            =   "#CritereModule.NOMEMPLOYE"
      TextAlign       =   0
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   118
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_code_paie_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.CODEPAIE"
      TextAlign       =   0
      Top             =   167
      VerticalCenter  =   0
      Visible         =   True
      Width           =   134
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_code_paie_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   20
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   25
      Text            =   ""
      Top             =   190
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView timesheetcdep_date_paie_IV
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   121
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   405647359
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   128
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_date_paie_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   0
      Text            =   "#PaieModule.DATEPAIE"
      TextAlign       =   0
      Top             =   107
      VerticalCenter  =   0
      Visible         =   True
      Width           =   121
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField timesheetcdep_date_paie_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   "#PaieModule.DATECUE"
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LimitText       =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   ""
      TextAlign       =   0
      Top             =   130
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_detail_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   0
      Text            =   "#PaieModule.DETAIL"
      TextAlign       =   0
      Top             =   227
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea timesheetcdep_detail_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   56
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "0"
      TabOrder        =   35
      Text            =   ""
      TextAlign       =   0
      Top             =   250
      VerticalCenter  =   0
      Visible         =   True
      Width           =   302
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_facturable_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.FACTURABLE"
      TextAlign       =   0
      Top             =   333
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_facturable_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   20
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   40
      Text            =   ""
      Top             =   357
      VerticalCenter  =   0
      Visible         =   True
      Width           =   88
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebLabel timesheetcdep_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   31
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   "ID"
      TextAlign       =   0
      Top             =   413
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_nas_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   38
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.NAS"
      TextAlign       =   0
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   50
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_nas_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   153
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   5
      Text            =   ""
      Top             =   70
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField timesheetcdep_nas_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   "#PaieModule.REQUIS"
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   70
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   82
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_nombre_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   208
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   0
      Text            =   "#PaieModule.HEURESMONTANT"
      TextAlign       =   0
      Top             =   167
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField timesheetcdep_nombre_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   "#PaieModule.DATECUE"
      Cursor          =   0
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   208
      LimitText       =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   30
      Text            =   ""
      TextAlign       =   0
      Top             =   190
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_paye_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   226
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.PAYE"
      TextAlign       =   0
      Top             =   333
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_paye_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   220
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1713690623"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   50
      Text            =   ""
      Top             =   357
      VerticalCenter  =   0
      Visible         =   True
      Width           =   88
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_projet_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   196
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.PROJET"
      TextAlign       =   0
      Top             =   107
      VerticalCenter  =   0
      Visible         =   True
      Width           =   72
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_projet_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   155
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   20
      Text            =   ""
      Top             =   131
      VerticalCenter  =   0
      Visible         =   True
      Width           =   170
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel timesheetcdep_revision_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   126
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   7
      Text            =   "#PaieModule.REVISION"
      TextAlign       =   0
      Top             =   333
      VerticalCenter  =   0
      Visible         =   True
      Width           =   80
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu timesheetcdep_revision_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   120
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1713690623"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   45
      Text            =   ""
      Top             =   357
      VerticalCenter  =   0
      Visible         =   True
      Width           =   88
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub alimListeDeroulante()
		  // Alimenter les listes déroulantes
		  loadDataEmploye
		  loadDataProjet
		  
		  timesheetcdep_code_paie_PopupMenu.loadData(Session.bdTechEol,"parametre", "parametre", "codepaieentree")
		  timesheetcdep_facturable_PopupMenu.loadData(Session.bdTechEol, "parametre", "parametre", "boolean")
		  timesheetcdep_revision_PopupMenu.loadData(Session.bdTechEol, "parametre", "parametre", "boolean")
		  timesheetcdep_paye_PopupMenu.loadData(Session.bdTechEol, "parametre", "parametre", "boolean")
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementEmploye()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listPaieDetail()
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  If Session.pointeurPaieSideBarContainer.sPaie = Nil Then
		    Session.pointeurPaieSideBarContainer.sPaie = new PaieClass()
		  End If
		  
		  If Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Creation" Then
		    Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Modification"
		    timesheetcdep_nas_TextField.Text = Session.pointeurPaieSideBarContainer.paieStruct.cle
		    Exit
		  End If
		  
		  // Alimenter les listes déroulantes
		  chargerLocalisationFait = False
		  alimListeDeroulante
		  
		  // Mode Modification, Lire les données
		  Dim paieRS As RecordSet = Session.pointeurPaieSideBarContainer.sPaie.loadDataByField(Session.bdTechEol, PaieModule.PAIETABLENAME, _
		  "timesheetcdep_id", str(Session.pointeurPaieSideBarContainer.paieStruct.id), "timesheetcdep_id")
		  
		  If paieRS <> Nil Then Session.pointeurPaieSideBarContainer.sPaie.LireDonneesBD(paieRS, PaieModule.PAIEPREFIX)
		  
		  paieRS.Close
		  paieRS = Nil
		  
		  // Remplir la liste déroulante secondaire et la positionner à la bonne valeur
		  //paie_cleetr_cle_PopupMenu.loadCleEtrangere(Session.bdTechEol,  PaieModule.PAIETABLENAME, ParametreModule.parametrePrefix,_
		  //Self.sPaie.paie_cleetr_nom, Self.sPaie.paie_cleetr_cle)
		  
		  
		  // Sauvegarde
		  sauvegardePaieCle = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_nas
		  
		  Session.pointeurPaieSideBarContainer.paieStruct.id = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_id
		  Session.pointeurPaieSideBarContainer.paieStruct.cle = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_nas
		  
		  Dim stest As PaieClass = Session.pointeurPaieSideBarContainer.sPaie
		  // Assignation des propriétés aux Contrôles
		  Session.pointeurPaieSideBarContainer.sPaie.assignPropertiesToControls(Self, PaieModule.PAIEPREFIX)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  // Paramètre 5 = Dictionnaire des propriétés
		  // Verrouillage de toutes les zones
		  GenControlModule.resetControls(Self, False, False, True)
		  // Déverrouillage de toutes les zones associées à la classe dont on passe le dictionnaire en paramètre
		  GenControlModule.resetControls(Self, False, False, False, Session.pointeurPaieSideBarContainer.sPaie.propertyDict)
		  
		  
		  // Verrouiller certaines zones
		  timesheetcdep_nas_TextField.Enabled = False
		  timesheetcdep_date_paie_TextField.Enabled = False
		  
		  // Déverrouiller les boutons
		  IVQuitter.Enabled = True
		  IVUpdate.Enabled = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub loadDataEmploye()
		  Dim recordSet As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM employe Order By employe_nom, employe_prenom "
		  
		  recordSet =  Session.bdTechEol.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  timesheetcdep_nas_PopupMenu.DeleteAllRows
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      timesheetcdep_nas_PopupMenu.AddRow(recordSet.Field( "employe_nom").StringValue + " " + recordSet.Field( "employe_prenom").StringValue)
		      // Mettre la valeur de la clé dans le RowTag
		      timesheetcdep_nas_PopupMenu.RowTag(timesheetcdep_nas_PopupMenu.ListCount-1) = recordSet.Field( "employe_nas").StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub loadDataProjet()
		  Dim recordSet As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM projet WHERE projet_statut = 'Travaux en cours'  Order By projet_no "
		  
		  recordSet =  Session.bdTechEol.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  timesheetcdep_projet_PopupMenu.DeleteAllRows
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      timesheetcdep_projet_PopupMenu.AddRow(recordSet.Field( "projet_no").StringValue + " " + recordSet.Field( "projet_titre").StringValue)
		      // Mettre la valeur de la clé dans le RowTag
		      timesheetcdep_projet_PopupMenu.RowTag(timesheetcdep_projet_PopupMenu.ListCount-1) = recordSet.Field( "projet_no").StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub majPaieDetail()
		  Dim messageErreur  As String = "Succes"
		  
		  If Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Creation" Then
		    alimListeDeroulante
		    // En mode création, mettre le tag_modif à 1 pour signaler un enregistrement ajouté 
		    Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_tag_modif = "1"
		    // Assignation des propriétés aux Contrôles
		    Session.pointeurPaieSideBarContainer.sPaie.assignPropertiesToControls(Self, PaieModule.PAIEPREFIX)
		  End If
		  
		  Session.pointeurPaieSideBarContainer.sPaie.assignControlToProperties(Self, PaieModule.PAIEPREFIX)
		  
		  // Traitement spécial pour le temps -> Remplacer le point par une virgule pour être consistant avec les champs numériques
		  //Session.sRepPale.reppale_temps = Replace(Session.sRepPale.reppale_temps,".",",")
		  
		  // Ajouter 1 à la valeur de la séquence  'form.timesheetcdep_id_seq' si on est en Création
		  If Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Creation" Then Session.bdTechEol.SQLExecute("SELECT NEXTVAL('form.timesheetcdep_id_seq')")  
		  // Écriture
		  messageErreur = Session.pointeurPaieSideBarContainer.sPaie.writeData(Session.bdTechEol, PaieModule.PAIETABLENAME, paieModule.paiePREFIX)
		  
		  If (messageErreur <> "Succes") Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageErreur
		    ML6probMsgBox.Show
		    Exit
		  End If
		  
		  // Réafficher la liste de paie si pas en création
		  If Session.pointeurPaieSideBarContainer.paieStruct.edit_mode <> "Creation"  Then
		    Session.pointeurPaieSideBarContainer.lancePopulatePaie(Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_id)
		  End If
		  
		  sauvegardePaieCle = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_nas
		  
		  Session.pointeurPaieSideBarContainer.paieStruct.id = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_id
		  Session.pointeurPaieSideBarContainer.paieStruct.cle = Session.pointeurPaieSideBarContainer.sPaie.timesheetcdep_nas
		  
		  If Session.pointeurPaieSideBarContainer.paieStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Modification"
		  
		  traiteQuitter
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteMAJ()
		  
		  If validationDesZonesOK = True Then
		    majPaieDetail
		    listPaieDetail
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteQuitter()
		  
		  Dim message As String = Session.pointeurPaieSideBarContainer.sPaie.cleanUserLocks(Session.bdTechEol)
		  If message <> "Succes" Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = message
		    ML6probMsgBox.Show
		  End If
		  Self.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validationDesZonesOK() As Boolean
		  GenControlModule.resetControls(Self, False, True, False)
		  
		  Dim messageRetour as String = ""
		  
		  
		  // Couper les zones à leur longeur maximum dans la BD
		  Session.pointeurPaieSideBarContainer.sPaie.coupeStringLongueurMaxBD(Self)
		  
		  // Vérification et traitement des zones numériques
		  messageRetour = Session.pointeurPaieSideBarContainer.sPaie.valideZoneNumerique(Self)
		  If messageRetour <> "Succes" Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageRetour
		    ML6probMsgBox.Show
		    Return False
		  End If
		  
		  // Validation
		  //messageRetour = Session.pointeurPaieSideBarContainer.sPaie.validerZones( _
		  //Session.bdTechEol, _
		  //paie_unite_no_TextField)
		  
		  If messageRetour <> "Succes" Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageRetour
		    ML6probMsgBox.Show
		    Return False
		  End If
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		chargerLocalisationFait As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardePaieCle As String
	#tag EndProperty


#tag EndWindowCode

#tag Events IVUpdate
	#tag Event
		Sub MouseEnter()
		  Me.Picture = EnregistrerOrange60x60
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  traiteMaj
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Me.Picture = Enregistrer60x60Menu
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVQuitter
	#tag Event
		Sub MouseExit()
		  Self.IVQuitter.Picture = FermerModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVQuitter.Picture = FermerModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  traiteQuitter
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timesheetcdep_code_paie_PopupMenu
	#tag Event
		Sub SelectionChanged()
		  Dim codePaieEntree As String = Me.RowTag(Me.listIndex)
		  If mid(codePaieEntree, 1, 1) = "p" Or mid(codePaieEntree, 1, 1) = "r" Then
		    Dim codePaieEntreeArray(-1) As String =  Split(Me.Text, "$")
		    timesheetcdep_nombre_TextField.Text = codePaieEntreeArray(1)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timesheetcdep_date_paie_IV
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    Dim cal As New ContainerWCP
		    cal.activeField  = timesheetcdep_date_paie_TextField
		    cal.initDate(Self, "")
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timesheetcdep_date_paie_TextField
	#tag Event
		Sub LostFocus()
		  Me.Text = Trim(Me.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timesheetcdep_nas_PopupMenu
	#tag Event
		Sub SelectionChanged()
		  changementEmploye
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events timesheetcdep_nombre_TextField
	#tag Event
		Sub LostFocus()
		  Me.Text = Trim(Me.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="chargerLocalisationFait"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Minimum Size"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizable"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"1 - Sheet"
			"2 - Palette"
			"3 - Modal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
