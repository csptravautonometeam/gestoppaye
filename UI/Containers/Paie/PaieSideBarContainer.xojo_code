#tag WebPage
Begin WebContainer PaieSideBarContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   550
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1146
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle PaieRectangle
      Cursor          =   0
      Enabled         =   True
      Height          =   550
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1622472703"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1146
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox PaieSideBarListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   10
      ColumnWidths    =   "7%,17%,7%,20%,10%,4%,20%,5%,5%,5%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   469
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#PaieModule.NAS	#PaieModule.EMPLOYE	Date	#PaieModule.PROJET	#PaieModule.CODEPAIE	#PaieModule.HEURESMONTANT	#PaieModule.DETAIL	#PaieModule.FACTURABLE	#PaieModule.REVISION	#PaieModule.PAYE"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   81
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1136
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModPaie
      AlignHorizontal =   0
      AlignVertical   =   3
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1066
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1238587391
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelPaie
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   952
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   415664127
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddPaie
      AlignHorizontal =   2
      AlignVertical   =   0
      Cursor          =   1
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   986
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1401872383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafPaie
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1101
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   2017697791
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVVisPaie
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1031
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1504260095
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   10
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin CritereContainerControl PaieSideBarContainerCritereContainerControl
      Cursor          =   0
      Enabled         =   True
      Height          =   81
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   0
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebButton AccesEmployeButton
      AutoDisable     =   False
      Caption         =   "#PaieModule.EMPLOYE"
      Cursor          =   0
      Enabled         =   False
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   952
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "-1"
      TabOrder        =   1
      Top             =   47
      VerticalCenter  =   0
      Visible         =   False
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  lancePopulatePaie
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub accesEmploye()
		  
		  Session.pointeurEmployeWebDialog =new EmployeWebDialog
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.employeStruct.no_ligne = 0
		  Session.pointeurEmployeWebDialog.EmployeSideBarDialog.populateEmploye
		  Session.pointeurEmployeWebDialog.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ajouterPaie()
		  Dim compteur As Integer
		  compteur = PaieSideBarListBox.RowCount  'Pour Debug
		  compteur = PaieSideBarListBox.LastIndex    'Pour Debug
		  compteur = PaieSideBarListBox.ListIndex ' Pour Debug
		  
		  PaieSideBarListBox.insertRow(compteur+1 _ 
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,0) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,1) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,2) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,3) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,4) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,5) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,6) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,7)_
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,8) _
		  ,PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,9))
		  // Faire pointer le curseur sur le nouvel enregistrement
		  PaieSideBarListBox.ListIndex = PaieSideBarListBox.ListIndex + 1
		  Session.pointeurPaieSideBarContainer.paieStruct.no_ligne = PaieSideBarListBox.ListIndex
		  Session.pointeurPaieSideBarContainer.paieStruct.cle = PaieSideBarListBox.Cell(PaieSideBarListBox.ListIndex,0)
		  Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement de paie
		  //Dim indexCritere As Integer = PaieSideBarContainerCritereContainerControl.TypePaiePopupMenu.ListIndex
		  //Dim classCritere As String = "toeo"
		  //If indexCritere <= 0 Then
		  //Self.sPaie = new AllEquipmentToolClass()
		  //Else
		  //classCritere = PaieSideBarContainerCritereContainerControl.TypePaiePopupMenu.RowTag(indexCritere)
		  //Self.sPaie = selectPaieClass(classCritere)
		  //End If
		  //Self.sPaie.equipm_unite_no = "T-"
		  //Self.sPaie.equipm_type = classCritere
		  //Dim dateTrav As Date = new Date()
		  //Self.sPaie.equipm_localisation_date = str(dateTrav.Year) + "-" +str(dateTrav.Month) + "-" + str(dateTrav.Day)
		  //Self.sPaie.equipm_statut = "A"
		  
		  Session.pointeurPaieSideBarContainer.paieStruct.id = 0  // indique que c'est un nouvel enregistrement
		  Self.sPaie.timesheetcdep_id = 0
		  Dim ML6PaieWebDialog As PaieWebDialog = New PaieWebDialog
		  ML6PaieWebDialog.majPaieDetail
		  ML6PaieWebDialog = Nil
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  lancePopulatePaie
		  
		  // Se mettre en mode lock
		  implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub changementPaie(idPaie_param As Integer)
		  // Si la valeur de idPaie est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idPaie_param  = 0 Then
		    //Session.pointeurPaieSideBarContainer.paieStruct.edit_mode = "Creation"
		  Else
		    Self.sPaie = Nil
		    Self.sPaie = New PaieClass
		    Dim stest As PaieClass = Self.sPaie
		    // Lecture
		    Self.sPaie.listDataByField(Session.bdTechEol, PaieModule.PAIETABLENAME, PaieModule.PAIEPREFIX,  _
		    "timesheetcdep_id", str(Session.pointeurPaieSideBarContainer.paieStruct.id), "timesheetcdep_id")
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub confirmDelete()
		  Dim qDialog As New QModal
		  qDialog.Show
		  
		  AddHandler qDialog.Dismissed, AddressOf supPaieConfirme
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  //Super.Constructor
		  
		  // Écraser les paramètres par défaut de la classe
		  
		  //Me.Width = 1200
		  //Me.LockLeft = True
		  //Me.LockRight = True
		  //Me.LockTop = True
		  //Me.LockBottom = True
		  //Me.Top = 0
		  //Me.Left = 0 
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function determineDetailOuSommaire() As Boolean
		  Dim sommaire As Boolean
		  Select Case PaieSideBarContainerCritereContainerControl.DetSomRadioGroup.SelectedCell.Left
		  Case 0
		    sommaire = False
		  Case 1
		    sommaire = True
		  End Select
		  
		  Return sommaire
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub generateButtonVisualize()
		  IVVisPaie.Visible = False
		  IVVisPaie.Enabled = False
		  
		  If PaieSideBarListBox.ListIndex = -1  Then Exit Sub
		  
		  // Si ce n'est pas un enregistrement photo, Sortir
		  If self.sPaie.timesheetcdep_code_paie <> "iphotocd" Then Exit Sub
		  
		  // Débloquer lorsque ce sera nécessaire
		  //Exit Sub
		  
		  IVVisPaie.Visible = True
		  IVVisPaie.Enabled = True
		  Dim lienVisualize As String = ""
		  
		  If Session.environnementProp = "Preproduction" Then  // Nous sommes en préProduction
		    lienVisualize = "http://" + HOSTPHOTOPREPROD + "/gestfin/comptedepense/" + mid(Self.sPaie.timesheetcdep_date_paie, 1, 4) + "/" + Self.sPaie.timesheetcdep_detail
		  ElseIf Session.environnementProp = "Test" Then  // Nous sommes en test
		    lienVisualize = "http://" + HOSTTEST + "/techeol/gestfin/comptedepense/" + mid(Self.sPaie.timesheetcdep_date_paie, 1, 4) + "/" + Self.sPaie.timesheetcdep_detail
		  Else
		    lienVisualize = "http://" + HOSTPHOTOPROD + "/gestfin/comptedepense/" + mid(Self.sPaie.timesheetcdep_date_paie, 1, 4) + "/" + Self.sPaie.timesheetcdep_detail
		  End If
		  
		  IVVisPaie.LinkAddTD(lienVisualize)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub implementLock()
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim paie_recherche As String = PaieModule.DBSCHEMANAME + "." + PaieModule.DBTABLENAME + "." + str(Self.sPaie.timesheetcdep_id)
		  message = Self.sPaie.check_If_Locked(Session.bdTechEol, paie_recherche)
		  If  message <> "Locked" And message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    message = PaieModule.ENREGVERROUILLEPAR + Self.sPaie.lockedBy
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sPaie.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sPaie.recordID = PaieModule.DBSCHEMANAME + "." +PaieModule.DBTABLENAME+ "." + str(Self.sPaie.timesheetcdep_id)
		  message = Self.sPaie.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  Dim ML6PaieWebDialog As PaieWebDialog = New PaieWebDialog
		  //Appeler la modification
		  ML6PaieWebDialog.listPaieDetail
		  ML6PaieWebDialog.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lancePopulatePaie(Optional timesheetcdep_id_param as Integer)
		  Dim sommaire As Boolean
		  Select Case PaieSideBarContainerCritereContainerControl.DetSomRadioGroup.SelectedCell.Left
		  Case 0
		    sommaire = False
		  Case 1
		    sommaire = True
		  End Select
		  
		  populatePaie(sommaire, timesheetcdep_id_param)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub loadImageLogo(image as WebImageView)
		  
		  //image.Picture = CartierRepPalesLogo350x100
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub modifierPaie()
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If PaieSideBarListBox.ListIndex <> -1 And PaieSideBarListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub modifierValeurRevision(timesheetcdep_id_param As Integer)
		  
		  Dim sPaie As  PaieClass = new PaieClass()
		  
		  // Mode Modification, Lire les données
		  Dim paieRS As RecordSet = sPaie.loadDataByField(Session.bdTechEol, PaieModule.PAIETABLENAME, _
		  "timesheetcdep_id", str(timesheetcdep_id_param), "timesheetcdep_id")
		  
		  If paieRS <> Nil Then sPaie.LireDonneesBD(paieRS, PaieModule.PAIEPREFIX)
		  
		  paieRS.Close
		  paieRS = Nil
		  
		  If sPaie.timesheetcdep_revision = "2" Then
		    sPaie.timesheetcdep_revision = "1"
		  Else
		    sPaie.timesheetcdep_revision = "2"
		  End
		  
		  Dim messageErreur  As String = sPaie.writeData(Session.bdTechEol, PaieModule.PAIETABLENAME, PaieModule.PAIEPREFIX)
		  
		  sPaie = Nil
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populatePaie(detSom As Boolean, Optional timesheetcdep_id_param as Integer)
		  If Self.sPaie = Nil Then
		    Self.sPaie = new PaieClass()
		  End If
		  
		  Dim listeSelection As Dictionary = PaieSideBarContainerCritereContainerControl.chargementListeSelection
		  
		  Dim paieRS As RecordSet
		  
		  // detSom = False -> Détail
		  // detSom = True -> Sommaire
		  If detSom Then
		    paieRS = Self.sPaie.loadDataPaieSommaire(Session.bdTechEol, listeSelection)
		  Else
		    paieRS = Self.sPaie.loadDataPaieDetail(Session.bdTechEol, listeSelection)
		  End If
		  
		  PaieSideBarListBox.DeleteAllRows
		  
		  // S'il n'y a rien, on sort
		  If paieRS = Nil Then Exit Sub
		  
		  // detSom = False -> Détail
		  // detSom = True -> Sommaire
		  If detSom Then
		    For i As Integer = 1 To paieRS.RecordCount
		      PaieSideBarListBox.AddRow(paieRS.Field("timesheetcdep_nas").StringValue, paieRS.Field("timesheetcdep_employe_nom").StringValue, _
		      paieRS.Field("timesheetcdep_date_paie").StringValue, paieRS.Field("timesheetcdep_projet_description").StringValue, _
		      paieRS.Field("timesheetcdep_code_paie_description").StringValue, ReplaceAll(Format(paieRS.Field("timesheetcdep_nombre_somme").DoubleValue, "0.00"),",","."), _
		      "", "", "", "")
		      
		      // Garder le numéro de ligne
		      PaieSideBarListBox.RowTag(PaieSideBarListBox.LastIndex) = i -1
		      // Garder l'id numéro du paie, cumul, donc pas de numéro
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,1) = 0
		      // Garder le type d'équipement
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,2) = paieRS.Field("timesheetcdep_nas").StringValue
		      // Garder l'id de la modification, pas de modif
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,3) = ""
		      
		      paieRS.MoveNext
		    Next
		  Else
		    For i As Integer = 1 To paieRS.RecordCount
		      PaieSideBarListBox.AddRow(paieRS.Field("timesheetcdep_nas").StringValue, paieRS.Field("timesheetcdep_employe_nom").StringValue, _
		      paieRS.Field("timesheetcdep_date_paie").StringValue, paieRS.Field("timesheetcdep_projet_description").StringValue, _
		      paieRS.Field("timesheetcdep_code_paie_description").StringValue, paieRS.Field("timesheetcdep_nombre").StringValue, _
		      paieRS.Field("timesheetcdep_detail").StringValue, paieRS.Field("timesheetcdep_facturable_description").StringValue, _
		      paieRS.Field("timesheetcdep_revision_description").StringValue, paieRS.Field("timesheetcdep_paye_description").StringValue)
		      
		      If paieRS.Field("timesheetcdep_tag_modif").StringValue <> "2" Then
		        For j As Integer = 0 To 9
		          PaieSideBarListBox.CellStyle(i-1, j) = RedTextLeft12
		        Next
		      End If
		      
		      // Garder le numéro de ligne
		      PaieSideBarListBox.RowTag(PaieSideBarListBox.LastIndex) = i -1
		      // Garder l'id numéro du paie
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,1) = paieRS.Field("timesheetcdep_id").IntegerValue
		      // Garder le type d'équipement
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,2) = paieRS.Field("timesheetcdep_nas").StringValue
		      // Garder l'id de la modification
		      PaieSideBarListBox.CellTag(PaieSideBarListBox.LastIndex,3) = paieRS.Field("timesheetcdep_tag_modif").StringValue
		      
		      paieRS.MoveNext
		    Next
		  End If
		  paieRS.Close
		  paieRS = Nil
		  
		  //Self.generateButtonVisualize
		  
		  // Positionnement par rapport à un numéro en particulier
		  If timesheetcdep_id_param <> 0 Then
		    For indexListBox As Integer = 0 To PaieSideBarListBox.RowCount - 1
		      If PaieSideBarListBox.CellTag(indexListBox,1) = timesheetcdep_id_param Then
		        Session.pointeurPaieSideBarContainer.paieStruct.no_ligne = indexListBox
		        PaieSideBarListBox.ListIndex  = indexListBox
		        Session.pointeurPaieSideBarContainer.paieStruct.id = PaieSideBarListBox.CellTag(Session.pointeurPaieSideBarContainer.PaieStruct.no_ligne,1)
		        Exit Sub
		      End If
		    Next
		    Exit Sub
		  End If
		  
		  // Positionnement à la ligne gardée en mémoire si la liste n'est pas vide
		  // detSom = False -> Détail
		  // detSom = True -> Sommaire
		  If Not detSom And PaieSideBarListBox.ListIndex  <> Session.pointeurPaieSideBarContainer.paieStruct.no_ligne And PaieSideBarListBox.RowCount > 0 Then
		    Dim v As Variant = PaieSideBarListBox.LastIndex
		    v = Session.pointeurPaieSideBarContainer.paieStruct.no_ligne
		    If Session.pointeurPaieSideBarContainer.paieStruct.no_ligne > PaieSideBarListBox.LastIndex  Then Session.pointeurPaieSideBarContainer.paieStruct.no_ligne = paieSideBarListBox.LastIndex
		    PaieSideBarListBox.ListIndex  = Session.pointeurPaieSideBarContainer.paieStruct.no_ligne
		    Session.pointeurPaieSideBarContainer.paieStruct.id = PaieSideBarListBox.CellTag(Session.pointeurPaieSideBarContainer.paieStruct.no_ligne,1)
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub resetIV()
		  IVModPaie.Picture = ModModal30x30
		  IVAddPaie.Picture = AjouterModal30x30
		  IVRafPaie.Picture = RefreshMenu30x30
		  IVDelPaie.Picture = DeleteModal30x30
		  IVVisPaie.Picture = vizfich30x30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function selectPaieClass(typePaie As String) As PaieClass
		  Dim paieClass As PaieClass
		  
		  //Select Case typePaie 
		  //
		  //Case "voit"
		  //Dim voitureClassInst As VoitureClass = new VoitureClass
		  //paieClass = voitureClassInst
		  //
		  //Case "cami"
		  //Dim camionClassInst As CamionClass = new CamionClass
		  //paieClass = camionClassInst
		  //
		  //Case "civi", "tsgs"
		  //Dim civCoulisseauClassInst As CivCoulisseauClass = new CivCoulisseauClass
		  //paieClass = civCoulisseauClassInst
		  //
		  //Case "detc", "durb", "durs", "erad", "mumg", "piam", "pmph", "thha", "thin", "towr", "maal"
		  //Dim eqOutilSelClassInst As EqOutilSelClass = new EqOutilSelClass
		  //paieClass = eqOutilSelClassInst
		  //
		  //Case "kits", "pltf"
		  //Dim kitPltfClassInst As KitPltfClass = new KitPltfClass
		  //paieClass = kitPltfClassInst
		  //
		  //Case "remo", "remf"
		  //Dim remClassInst As RemClass = new RemClass
		  //paieClass = remClassInst
		  //
		  //Case "damu"
		  //Dim damClassInst As DamClass = new DamClass
		  //paieClass = damClassInst
		  //
		  //Case "pelj", "defb", "extc"
		  //Dim peDeExClassInst As PeDeExClass = new PeDeExClass
		  //paieClass = peDeExClassInst
		  //
		  //Case "bore", "trai"
		  //Dim borTraiClassInst As BorTraiClass = new BorTraiClass
		  //paieClass = borTraiClassInst
		  //
		  //Case "gene"
		  //Dim genClassInst As GenClass = new GenClass
		  //paieClass = genClassInst
		  //
		  //Case "motn"
		  //Dim motClassInst As MotClass = new MotClass
		  //paieClass = motClassInst
		  //
		  //Case "trec"
		  //Dim tourClassInst As TourClass = new TourClass
		  //paieClass = tourClassInst
		  //
		  //Case "vrad", "tens"
		  //Dim vradTensClassInst As VradTensClass = new VradTensClass
		  //paieClass = vradTensClassInst
		  //
		  //Case "toeo"
		  //Dim allEquipmentToolClassInst As AllEquipmentToolClass = new AllEquipmentToolClass
		  //paieClass = allEquipmentToolClassInst
		  //
		  //Case Else // par défaut
		  //Dim equiClass As PaieClass = new PaieClass
		  //paieClass = equiClass
		  //
		  //End Select
		  
		  Return paieClass
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub supPaieConfirme(Sender As QModal)
		  Dim messageErreur as String = "Succes"
		  //Réponse en provenance du modal 
		  If Sender.dialogResponse = 1 Then
		    
		    // L'id  est contenu dans la variable Session.pointeurPaieSideBarContainer.paieStruct.id
		    messageErreur = Self.sPaie.supprimerDataByField(Session.bdTechEol, PaieModule.PAIETABLENAME, "timesheetcdep_id",str(Session.pointeurPaieSideBarContainer.paieStruct.id))
		    
		    If messageErreur <> "Succes" Then
		      Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		      pointeurProbMsgModal.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageErreur
		      pointeurProbMsgModal.Show
		    Else
		      // Renuméroter et Réafficher la liste des projets
		      Self.lancePopulatePaie
		    End If
		    
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub supPaieDetail()
		  
		  // Vérifier si un enregistrement peut être supprimé
		  Dim messageErreur as String = "Succes"
		  //messageErreur =  Self.sPaie.validerSuppression(Session.bdTechEol, Self.sPaie.timesheetcdep_id)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = PaieModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim paie_recherche As String = PaieModule.DBSCHEMANAME + "." + PaieModule.DBTABLENAME + "." + str(Self.sPaie.timesheetcdep_id)
		  messageErreur = Self.sPaie.check_If_Locked(Session.bdTechEol, paie_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    messageErreur = PaieModule.ENREGVERROUILLEPAR + Self.sPaie.lockedBy
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  
		  confirmDelete
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub unlockPaie()
		  Dim lockedRowIDBis As integer = Session.sauvegardeLockedRowID
		  Dim message As String = Self.sPaie.unLockRow(Session.bdTechEol)
		  If  message <> "Succes" Then
		    afficherMessage(message)
		  End If
		  
		  Self.sPaie.lockEditMode = False
		  Self.Close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		paieStruct As PaieStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sPaie As PaieClass
	#tag EndProperty


	#tag Structure, Name = paieStructure, Flags = &h0
		cle As String*30
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		  statut As String*1
		type As String*4
	#tag EndStructure


#tag EndWindowCode

#tag Events PaieSideBarListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Ancun paie n'a été sélectionné, réinitialiser les champs
		    //ClearPaiesFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurPaieSideBarContainer.paieStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurPaieSideBarContainer.paieStruct.id = Me.CellTag(Me.ListIndex,1)
		    Dim valeur As Integer = Me.CellTag(Me.ListIndex,1)
		    changementPaie(Me.CellTag(Me.ListIndex,1))
		    generateButtonVisualize
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If determineDetailOuSommaire = False Then
		    resetIV
		    modifierPaie
		  End If
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModPaie
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = ModModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If determineDetailOuSommaire = False And PaieSideBarListBox.ListIndex <> -1 And PaieSideBarListBox.RowCount > 0 Then
		    resetIV
		    Dim firstSelectedRow As Integer  = -1 // Will hold the index of first selected row
		    For row As Integer = 0 To PaieSideBarListBox.RowCount - 1 // Rows are zero-based
		      If PaieSideBarListBox.Selected(row) Then
		        If firstSelectedRow = -1 Then firstSelectedRow = row
		        modifierValeurRevision(PaieSideBarListBox.CellTag(row,1))
		      End If
		    Next
		    Self.populatePaie(determineDetailOuSommaire, PaieSideBarListBox.CellTag(firstSelectedRow,1))
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelPaie
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = DeleteModalOrange
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If determineDetailOuSommaire = False Then
		    supPaieDetail
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddPaie
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = AjouterModalOrange30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If determineDetailOuSommaire = False Then
		    ajouterPaie
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafPaie
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Me.Picture = RefreshMenuOra30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  resetIV
		  lancePopulatePaie
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVVisPaie
	#tag Event
		Sub MouseExit()
		  resetIV
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  resetIV
		  Self.IVVisPaie.Picture =vizfichOra30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  // Le bouton est initialisé dans generateButtonVisualize
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AccesEmployeButton
	#tag Event
		Sub Action()
		  If PaieSideBarListBox.ListIndex <> -1 And PaieSideBarListBox.RowCount > 0 Then
		    accesEmploye
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
