#tag Module
Protected Module CritereModule
	#tag Constant, Name = CODEPAIE, Type = String, Dynamic = True, Default = \"Code de paie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code de paie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code de paie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pay code"
	#tag EndConstant

	#tag Constant, Name = COMMENTAIRE, Type = String, Dynamic = True, Default = \"Commentaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Commentaire"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Commentaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Comment"
	#tag EndConstant

	#tag Constant, Name = DATECREATION, Type = String, Dynamic = True, Default = \"Cr\xC3\xA9ation <\x3D", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cr\xC3\xA9ation <\x3D"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Creation <\x3D"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cr\xC3\xA9ation <\x3D"
	#tag EndConstant

	#tag Constant, Name = DATEDEBUT, Type = String, Dynamic = True, Default = \"Date d\xC3\xA9but", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date d\xC3\xA9but"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Begin date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date d\xC3\xA9but"
	#tag EndConstant

	#tag Constant, Name = DATEFIN, Type = String, Dynamic = True, Default = \"Date fin", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date fin"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"End date"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date fin"
	#tag EndConstant

	#tag Constant, Name = DATEPAIE, Type = String, Dynamic = True, Default = \"Date paie <\x3D", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Date paie <\x3D"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pay date <\x3D"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Date paie <\x3D"
	#tag EndConstant

	#tag Constant, Name = DEPENSE, Type = String, Dynamic = True, Default = \"D\xC3\xA9pense", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9pense"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Expense"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9pense"
	#tag EndConstant

	#tag Constant, Name = DETAIL, Type = String, Dynamic = True, Default = \"D\xC3\xA9tail", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9tail"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Detail"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9tail"
	#tag EndConstant

	#tag Constant, Name = FILTRES, Type = String, Dynamic = True, Default = \"Filtres", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Filtres"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Filtres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Filters"
	#tag EndConstant

	#tag Constant, Name = HEURE, Type = String, Dynamic = True, Default = \"Heure", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Heure"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Hour"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Heure"
	#tag EndConstant

	#tag Constant, Name = IMAGE, Type = String, Dynamic = True, Default = \"Image", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Image"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Picture"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Image"
	#tag EndConstant

	#tag Constant, Name = NOMEMPLOYE, Type = String, Dynamic = True, Default = \"Nom de l\'employ\xC3\xA9", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Nom de l\'employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom de l\'employ\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Employee name"
	#tag EndConstant

	#tag Constant, Name = NONREVISE, Type = String, Dynamic = True, Default = \"Non r\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Non r\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Not reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Non r\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = PENSION, Type = String, Dynamic = True, Default = \"Pension", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Pension"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Pension"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pension"
	#tag EndConstant

	#tag Constant, Name = PHOTO, Type = String, Dynamic = True, Default = \"Photo", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Photo"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Photo"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Photo"
	#tag EndConstant

	#tag Constant, Name = RAPPORT, Type = String, Dynamic = True, Default = \"Rapport", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Rapport"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Report"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Rapport"
	#tag EndConstant

	#tag Constant, Name = REPAS, Type = String, Dynamic = True, Default = \"Repas", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Repas"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Meal"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Repas"
	#tag EndConstant

	#tag Constant, Name = REVISE, Type = String, Dynamic = True, Default = \"R\xC3\xA9vis\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vis\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Reviewed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vis\xC3\xA9"
	#tag EndConstant

	#tag Constant, Name = REVISION, Type = String, Dynamic = True, Default = \"R\xC3\xA9vision", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"R\xC3\xA9vision"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Revision"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"R\xC3\xA9vision"
	#tag EndConstant

	#tag Constant, Name = SOMMAIRE, Type = String, Dynamic = True, Default = \"Sommaire", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sommaire"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Summary"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sommaire"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
